<?php
/**
 * Class ContainerTest
 *
 * @package Pa_Example
 */

use Paexample\Plugin;
use Paexample\Posttype;

class PluginTest extends \PHPUnit\Framework\TestCase {


	public function tearDown() {
        Mockery::close();
        $this->plugin = new Plugin();
    }
    /**
     * Test that parameters get passed to the container
     */
	public function test_params_getting_passed() {
		
		$this->plugin['param'] = 'value';
		$this->assertEquals('value', $this->plugin['param']);	
	}
	/**
	 * test that the run mthod gets run from the plugin container
	 */
	public function test_service_being_run(){
		$action = $this->getMockBuilder('\Paexample\Posttype')
			->disableOriginalConstructor()
			->setMethods( ['run'] )
			->getMock();

		$action->expects( $this->once() )->method('run')->willReturn('foo');
	
		$action->run();

		$plugin = new Plugin();
		$plugin['posttype_properties'] = [
			'name' => 'article',	
		]; 
		$plugin['posttype'] = function ($c) {
				return new \Paexample\Posttype( $c['posttype_properties'] );
		};
		$plugin->run();
	}
}
