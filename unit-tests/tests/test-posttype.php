<?php
/**
 * Class PosttypeTest
 *
 * @package Pa_Example
 */

use Pimple\Container;
use Paexample\Posttype;

class PosttypeTest extends \PHPUnit\Framework\TestCase {


	public function setUp() {
	 	\WP_Mock::setUp();
	 	$this->properties = [
			'name' => 'article',
			'prefix' => 'pa'
		];
	 	$this->posttype = new Posttype($this->properties);
	} 
	/**
	 * check properties are set
	 */
	public function test_no_properties_exception() {

		$this->expectException('Exception');
		$posttype = new Posttype();

	}

	public function test_properties_getting_set() {

		$this->assertEquals($this->properties, $this->posttype->posttype_properties);

	}
	public function test_hooks_added(){

		\WP_Mock::expectActionAdded( 'init', [$this->posttype, 'register_posttype'], 10 );

		$this->posttype->run();

	}
	/*public function test_action_function_does_something(){


		\WP_Mock::passthruFunction( 'did_action', array( 'times' => 1 ) );
		\WP_Mock::passthruFunction( 'get_post_type_object', array( 'times' => 1 ) );
		\WP_Mock::passthruFunction( 'get_taxonomies', array( 'times' => 1 ) );

		$actual = $this->posttype->register_posttype( array(  'name'=>'article'  ) );

       	$this->assertInstanceOf(Extended_CPT::class, $actual);

	}*/

	public function tearDown() {
     	$this->addToAssertionCount(
        	\Mockery::getContainer()->mockery_getExpectationCount()
        );
 		\WP_Mock::tearDown();
 	} 

}