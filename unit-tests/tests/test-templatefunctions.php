<?php 

require_once dirname(__FILE__) . '/../../public/template-functions.php';

class TemplateTest extends \PHPUnit\Framework\TestCase {

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}
	public function test_no_post_id(){

		$expected = null;

		\WP_Mock::passthruFunction( 'get_post_meta' );

		$actual = get_the_keywords( );

		$this->assertEquals($expected, $actual);

	}
	public function test_no_global_post(){
		global $post;

		$post = new \stdClass;
		$post->ID = 42;

		$expected = ['Top Gear','TOP GEAR'];

		\WP_Mock::userFunction( 'get_post_meta', array(
			'args' 		=> [ 42, 'pa_keywords', true ],
			'times'		=> 1,
			'return' 	=> 'Top Gear;TOP GEAR' 
		) );

		$actual = get_the_keywords( );

		$this->assertEquals($expected, $actual);
	}

	public function test_get_the_keywords(){

		$expected = ['Top Gear','TOP GEAR'];

		\WP_Mock::userFunction( 'get_post_meta', array(
			'args' 		=> [ 42, 'pa_keywords', true ],
			'times'		=> 1,
			'return' 	=> 'Top Gear;TOP GEAR' 
		) );

		$actual = get_the_keywords( 42 );

		$this->assertEquals($expected, $actual);
	}
	public function test_the_keywords(){


		$expected = 'Top Gear, TOP GEAR';

		\WP_Mock::userFunction( 'get_post_meta', array(
			'args' 		=> [ 42, 'pa_keywords', true ],
			'times'		=> 1,
			'return' 	=> 'Top Gear;TOP GEAR' 
		) );

		$this->expectOutputString( $expected );

		the_keywords( 42 );


	}


}