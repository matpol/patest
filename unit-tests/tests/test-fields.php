<?php
/**
 * Class PosttypeTest
 *
 * @package Pa_Example
 */

use Pimple\Container;
use Paexample\Fields;

class FieldsTest extends \PHPUnit\Framework\TestCase {


	public function setUp() {
	 	\WP_Mock::setUp();
	 	$this->properties = [
			'metabox' 		=> [],
			'fields' 		=> [],
		];
	 	$this->fields = new Fields($this->properties);
	}

	/**
	 * check properties are set
	 */
	public function test_no_properties_exception() {

		$this->expectException('Exception');
		$fields = new Fields();

	}
 

	public function test_properties_getting_set() {

		$this->assertEquals( $this->properties, $this->fields->field_properties );
		
	}
	public function test_props_have_keys(){
	

		$this->assertArrayHasKey('metabox', $this->fields->field_properties);
		$this->assertArrayHasKey('fields', $this->fields->field_properties);
		


	}
	public function test_hooks_added(){

		\WP_Mock::expectActionAdded( 'cmb2_admin_init', [$this->fields, 'register_fields'], 10 );

		$this->fields->run();

	}
	public function tearDown() {
     	$this->addToAssertionCount(
        	\Mockery::getContainer()->mockery_getExpectationCount()
        );
 		\WP_Mock::tearDown();
 	} 

}