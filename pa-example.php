<?php
/*
Plugin Name: PA Example
description: A test plugin
Version: 1.0.0
License: GPL-2.0+
*/

require_once __DIR__ . '/vendor/autoload.php';

require_once __DIR__ . '/vendor/cmb2/init.php';
require_once __DIR__ .'/fields/cmb2-field-type-tags/cmb2-field-type-tags.php';
require_once __DIR__ .'/public/template-functions.php';

use Paexample\Plugin;
use Paexample\Posttype;
use Paexample\Fields;



add_action( 'plugins_loaded', 'paexample_init' ); 

function paexample_init() {

	$plugin = new Plugin(); 
	$plugin['path'] = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
	$plugin['url'] = plugin_dir_url( __FILE__ );
	$plugin['prefix'] = 'pa';
	$plugin['version'] = '1.0.0';
	$plugin['posttype_properties'] = [
		'name' => 'article',	
	];
	$plugin['field_properties'] = [
		'metabox' => [
			'id' => 'keywords',
			'title' => 'Keywords',
			'object_type' => $plugin['posttype_properties']['name']
		],
		'fields' => [
			[
				'name'       => __( 'Add Keywords', 'patest' ),
				'id'         => $plugin['prefix'] . '_keywords',
				'type'       => 'tags',
			]

		]
	];
	$plugin['posttype'] = function ($c) {
		return new Posttype( $c['posttype_properties'] );
	};
	$plugin['fields'] = function ($c) {
		return new Fields( $c['field_properties'] );
	};

	$plugin->run();

}

