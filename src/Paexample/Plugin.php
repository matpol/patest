<?php

/**
* Plugin class
*
* @package    paexample
* @subpackage plugin
* @author     Matthew Pollard
* @version    1.0.0
* 
*/

namespace Paexample;


class Plugin extends \Pimple\Container{
	
	
	/**
	 * run the runs
	 */
	public function run(){ 
		foreach( $this->keys() as $key){
			$content = $this[$key];
			if( is_object( $content ) ){
				$reflection = new \ReflectionClass( $content );
				if( $reflection->hasMethod( 'run' ) ){
					$content->run();
				}
			}
		}
	}
}