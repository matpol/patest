<?php

/**
* Fields class
*
* @package    paexample
* @subpackage fields
* @author     Matthew Pollard
* @version    1.0.0
* 
*/

namespace Paexample;

class Fields extends Base{
	
	public function __construct( $field_properties = null ){

		if( ! $field_properties ) {
			throw new \Exception('Fields parameters are not set');
		}

		$this->field_properties =  $field_properties;
	
		
	}

	public function run(){
		add_action( 'cmb2_admin_init',  [ $this, 'register_fields' ], 10 );	
	}
	/**
	  * add the metabox using CMB2
	  * slightly modified field type from:
	  * @see https://github.com/florianbeck/cmb2-field-type-tags
	  */ 
	// @codeCoverageIgnoreStart
	public function register_fields() {
		
		$cmb = new_cmb2_box( array(
			'id'            =>  $this->field_properties['metabox']['id'],
			'title'         => __( $this->field_properties['metabox']['title'], 'patest' ),
			'object_types'  => array(  $this->field_properties['metabox']['object_type'] ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => false,
			'fields' 		=> $this->field_properties['fields']
			) 
		);
		
	}
	// @codeCoverageIgnoreEnd

}