<?php

/**
* Posttype class
*
* @package    paexample
* @subpackage posttype
* @author     Matthew Pollard
* @version    1.0.0
* 
*/

namespace Paexample;

class Posttype extends Base{
	protected $postype_properties;

	public function __construct( $posttype_properties = null ){

		if( ! $posttype_properties ) {
			throw new \Exception('Posttype parameters are not set');
		}
    
    	$this->posttype_properties = $posttype_properties;	
  	}

  	public function run(){
  		add_action( 'init', [ $this, 'register_posttype' ], 10 );
  	}
	/**
	 * add the custom post type
	 */
	// @codeCoverageIgnoreStart
	public function register_posttype($properties = null){
		if( ! $properties){
			$properties =  $this->posttype_properties;
		}
	
		$cpt = new \Extended_CPT(  $properties['name'] );
		error_log(print_r($cpt,true));
		return $cpt;
	
	}
	// @codeCoverageIgnoreEnd


}