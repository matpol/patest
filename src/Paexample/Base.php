<?php

/**
* Base class
*
* @package    paexample
* @subpackage base
* @author     Matthew Pollard
* @version    1.0.0
* 
*/

namespace Paexample;

abstract class Base{

 	abstract protected function run();

}