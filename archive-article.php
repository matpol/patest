<?php get_header(); ?>

<div class="wrap">

	<?php 

		$args = array(
			'post_type' 	=> 'article',
			'post_status'	=> 'publish'
		);

		//simple way to add a search term
		$keyword = isset($_GET['keyword'])? $_GET['keyword'] : null;

		//if there is term add the meta query
		if(! empty($keyword)) {
			$args['meta_query'] =  array(
		        array(
		            'key'       => 'pa_keywords',
		            'value'     => $keyword,
		            'compare'   => 'LIKE',
		        )
	    	);
		}

		$custom_query = new WP_Query($args); 

	?>
	<form method="get" action="">
		<p>
			<label>Keyword Search:</label>
			<input type="text"  name="keyword" value="" id="keyword"/>
		</p>
		<button type="submit">Search</button>
	</form>
	<?php if ( have_posts() ) : ?>
		<?php while($custom_query->have_posts()) : $custom_query->the_post(); ?>
			<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<?php the_content(); ?>
				<p><strong>Keywords:</strong> <?php the_keywords(); ?></p>
			</div>
		<?php endwhile; ?>
	<?php else : ?>

	<?php endif; ?>
	<?php wp_reset_postdata(); // reset the query ?>

</div>

<?php get_footer();