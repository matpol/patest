<?php

if( ! function_exists( 'get_the_keywords' ) ) {

	/**
	 * get the keyword from the post
	 * @return array
	 */
	function get_the_keywords($postid = null){
		global $post;
		if(! $postid ){
			if(isset( $post->ID)){
				$postid = $post->ID;
			}
		}

		if( $postid ){
			return explode( ';', get_post_meta( $postid, 'pa_keywords', true ) );	
		}

		
	}
}

if( ! function_exists( 'the_keywords' ) ) {
	/**
	 * display the keywords as string with cmmas
	 */
	function the_keywords($postid = null){
		echo implode( ', ', get_the_keywords( $postid ) );
	}
}